// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */
    //get num test
    string s;
    getline(cin, s);
    int num_test;
    num_test = stoi(s);
    //get empty line
    getline(cin, s);
    int i = 0;
    while (i < num_test) {
        my_allocator<double, 1000> temp;
        string newS;
        while (getline(cin, newS)) {
            if (newS.compare("\r") == 0 || newS.empty()) {
                break;
            }
            else {
                int current = stoi(newS);

                if (current > 0) {
                    temp.allocate(current);
                }
                if (current < 0) {
                    my_allocator<double, 1000>::iterator iter = temp.begin();
                    int k = 0;
                    int m = 0;
                    while (k < -current) {
                        m++;
                        if (*iter < 0) {
                            k++;
                        }
                        if (k == -current) {
                            break;
                        }
                        iter++;
                    }

                    char* point = (reinterpret_cast<char*>(&(*iter)));
                    point = point + 4;
                    temp.deallocate(reinterpret_cast<double*>(point), (-(*iter))/sizeof(double));
                }
            }
        }
        my_allocator<double, 1000>::iterator iter2 = temp.begin();
        int* start = &(*iter2);
        while (true) {
            cout << *iter2;
            try {
                ++iter2;
                cout << " ";
            }
            catch(int e) {
                break;
            }

        }

        cout << "";
        cout << endl;
        ++i;
    }

    return 0;
}
