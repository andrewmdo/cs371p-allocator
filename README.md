# CS371p: Object-Oriented Programming Allocator Repo

* Name: Andrew Do

* EID: amd5637

* GitLab ID: amdrewmdo

* HackerRank ID: andrew_do

* Git SHA: 4fe660a49f22e28bd1c64f0fcb4f47cc64c8e7ef

* GitLab Pipelines: https://gitlab.com/andrewmdo/cs371p-allocator/-/pipelines

* Estimated completion time: 10 Hours

* Actual completion time: 15 Hours

* Comments: (any additional comments you have)
