// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <cmath>
#include "gtest/gtest.h"
// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& a, const iterator& b) {
            bool ret = a._p == b._p;
            return ret;
        }                                           // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;
        int* start;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
            start =p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            int copy = *_p;
            //creates copy and sets right sentinel
            char* pointer = reinterpret_cast<char*> (_p);
            pointer = pointer + std::abs(*reinterpret_cast<int*> (_p)) + 1 * sizeof(int);
            *reinterpret_cast<int*>(pointer) = *_p;
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            //get to the next left sentinal
            char* pointer = reinterpret_cast<char*> (_p);
            pointer = pointer + std::abs(*reinterpret_cast<int*> (_p)) + 2 * sizeof(int);
            _p = reinterpret_cast<int*> (pointer);
            if (_p - start >= 250) {
                throw -1;
            }
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            //decrement to the previous left sentinal
            char* pointer = reinterpret_cast<char*> (_p);
            char* leftRightSent = pointer - sizeof(int);
            pointer = pointer - (2 * sizeof(int)) - std::abs(*reinterpret_cast<int*>(leftRightSent));
            _p = reinterpret_cast<int*>(pointer);
            assert(valid());
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& a, const const_iterator& b) {
            bool ret = a._p == b._p;
            return ret;
        }                                                          // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }                 // replace!

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            //same as other iterator
            const char* pointer = reinterpret_cast<const char*> (_p);
            pointer = pointer + std::abs(*reinterpret_cast<const int*> (_p)) + 2 * sizeof(int);
            _p = reinterpret_cast<const int*> (pointer);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            //same as iterator
            const char* pointer = reinterpret_cast<const char*> (_p);
            const char* leftRightSent = pointer - sizeof(int);
            pointer = pointer - (2 * sizeof(int)) - std::abs(*reinterpret_cast<const int*>(leftRightSent));
            _p = reinterpret_cast<const int*>(pointer);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----
    FRIEND_TEST(AllocatorFixture, valid0);
    FRIEND_TEST(AllocatorFixture, valid1);
    /**
     * O(1) in space
     * O(n) in time
     * Checks if the sentinals match and that there are no free blocks next to each other
     */
    bool valid () const {
        const_iterator iter = begin();
        int prev =  0;
        int current = *iter;
        //go through all the blocks
        while (true) {
            const int* currentPointer = &(*iter);

            //end of blocks return true
            if (reinterpret_cast<const char*>(&(*iter)) - &a[0] >= N) {
                return true;
            }

            //if the last one and this one are both freed it is not valid
            if (prev > 0 && current > 0) {
                return false;
            }

            //find the right sentinel
            const int* rightSent =  currentPointer + std::abs(current)/sizeof(int) + 1;

            //if the two sentinals don't match
            if (*currentPointer != *rightSent) {
                return false;
            }

            ++iter;

            // set current and prev
            prev = current;
            current = *iter;
        }
        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if (N < sizeof(T) + (2 * sizeof(int))) {
            throw std::bad_alloc();
        }

        //set the sentinels
        get_pointer(0) = N - (2 * sizeof(int));
        get_pointer(N - sizeof(int)) = get_pointer(0);
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    int& get_pointer(int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {

        //space needed
        int minSpaceNeded =  (n * sizeof(T)) + sizeof(int) * 2;
        int i = 0;

        //loop through blocks
        while (i < N) {
            //get value of left sentinel
            int currentValue = get_pointer(i);

            //get space needed
            int currentSpace = std::abs(currentValue) + sizeof(int) * 2;

            //if we found a free block we can use
            if(currentValue > 0 && currentSpace >= minSpaceNeded) {

                //if the next block is not legal, allocate entire
                int spaceLeft = currentSpace - minSpaceNeded;
                if (spaceLeft < (sizeof(T) + (2 * sizeof(int))) && spaceLeft >= 0) {
                    get_pointer(i) = -currentValue;
                    get_pointer(i + currentValue + sizeof(int)) = -currentValue;

                }

                //else allocate only what is necessary
                else {

                    //set original
                    int firstValue = n * sizeof(T);
                    get_pointer(i) = -firstValue;

                    get_pointer(i + firstValue + sizeof(int)) = -firstValue;
                    //set next
                    int secondValue =  currentValue - (n * sizeof(T)) - (2 * sizeof(int));
                    get_pointer(i + firstValue + 2*sizeof(int)) = secondValue;
                    get_pointer(i + firstValue + 3*sizeof(int) + secondValue) = secondValue;
                }
                return reinterpret_cast<T*>(&a[i + sizeof(int)]);
            }
            else {
                // go to next block
                i += (2 * sizeof(int)) + std::abs(currentValue);
            }
        }

        // if i is too big then we could not alloc it.
        if (i >= N) {
            throw std::bad_alloc();
        }

        return nullptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * Get the pointer to something that was allocated and deallocates n T's
     * n must be the same as what was allocated
     */
    void deallocate (pointer p, size_type n) {
        char* pointer = reinterpret_cast<char*> (p);
        int index = 0;
        int found = false;

        //find the pointer in the list
        while(index < N && !found) {
            if (&a[index] == pointer) {
                found = true;
            }
            else {
                index++;
            }
        }

        // decrement to get sentinel
        if (found) {
            index = index - sizeof(int);
        }

        // if we did find it block could not be founds
        if (!found) {
            throw std::invalid_argument("couldn't find the block");
        }

        // get value at block
        int currentValue = get_pointer(index);

        // can deallocate freed block
        if (currentValue > 0) {
            throw std::invalid_argument("block is already free" + currentValue);
        }

        // make sure n is matched
        if (currentValue != (-1 * n * sizeof(T))) {
            int i =  (-1 * n * sizeof(T));
            throw std::invalid_argument("wrong pointer " + i);
        }


        currentValue = std::abs(currentValue);

        //set the current block
        int leftSentIndex = index;
        int rightSentIndex = leftSentIndex + std::abs(currentValue) + sizeof(int);
        get_pointer(leftSentIndex) = currentValue;
        get_pointer(rightSentIndex) = currentValue;

        //now need to merge
        int rightBlockStart = rightSentIndex + sizeof(int);

        //if there is a right block
        if (rightBlockStart < N) {
            int rightValue = get_pointer(rightBlockStart);

            //if right is free merge it, and get new right sentinel for the new block
            if (rightValue > 0) {
                rightSentIndex = rightSentIndex + 2 * sizeof(int) + rightValue;
                currentValue += rightValue + 2 * sizeof(int);
                get_pointer(leftSentIndex) = currentValue;
                get_pointer(rightSentIndex) = currentValue;
            }
        }

        //now do the same for the left side
        int leftBlockEnd = leftSentIndex - sizeof(int);

        //if there actually is a left block
        if (leftBlockEnd > 0) {
            int leftValue = get_pointer(leftBlockEnd);
            leftSentIndex = leftBlockEnd - sizeof(int) - leftValue;

            //if left is free merge, and get new left sentinel for new block
            if (leftValue > 0) {
                currentValue += leftValue + 2 * sizeof(int);
                get_pointer(leftSentIndex) = currentValue;
                get_pointer(rightSentIndex) = currentValue;
            }
        }
        assert(valid());

    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
