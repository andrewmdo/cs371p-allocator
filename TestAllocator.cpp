// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// uncomment
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                          // read/write
    ASSERT_EQ(x[0], 992);
}                                         // fix test


TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);
}                                         // fix test

TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    const pointer    b = x.allocate(5);
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], 964);
    ASSERT_EQ(x[996], 964);
}                                         // fix test


TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                      // read-only
    const pointer    b = x.allocate(5);
    x.deallocate(b, 5);
    // const pointer    a = x.allocate(10);
    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
}                                         // fix test


TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                      // read-only
    const pointer    b = x.allocate(5);
    const pointer    a = x.allocate(10);
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], -40);
    ASSERT_EQ(x[72], -40);
    ASSERT_EQ(x[76], 916);
    ASSERT_EQ(x[996], 916);
}                                         // fix test

TEST(AllocatorFixture, test7) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                      // read-only
    const pointer    b = x.allocate(5);
    x.deallocate(b, 5);
    allocator_type::iterator iter = x.begin();
    // const pointer    a = x.allocate(10);
    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
    ASSERT_EQ(*iter, 992);
}

TEST(AllocatorFixture, test8) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                      // read-only
    const pointer    b = x.allocate(5);
    const pointer    a = x.allocate(10);
    allocator_type::iterator iter = x.begin();
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], -40);
    ASSERT_EQ(x[72], -40);
    ASSERT_EQ(x[76], 916);
    ASSERT_EQ(x[996], 916);
    ASSERT_EQ(*iter, -20);
    ++iter;
    ASSERT_EQ(*iter, -40);
    ++iter;
    ASSERT_EQ(*iter, 916);
}

TEST(AllocatorFixture, test9) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                      // read-only
    const pointer    b = x.allocate(5);
    const pointer    a = x.allocate(10);
    allocator_type::iterator iter = x.begin();
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], -40);
    ASSERT_EQ(x[72], -40);
    ASSERT_EQ(x[76], 916);
    ASSERT_EQ(x[996], 916);
    ASSERT_EQ(*iter, -20);
    ++iter;
    ASSERT_EQ(*iter, -40);
    ++iter;
    ASSERT_EQ(*iter, 916);
    --iter;
    ASSERT_EQ(*iter, -40);
    --iter;
    ASSERT_EQ(*iter, -20);
}

TEST(AllocatorFixture, test10) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                      // read-only
    const pointer    b = x.allocate(5);
    const pointer    a = x.allocate(10);
    allocator_type::iterator iter = x.begin();
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], -40);
    ASSERT_EQ(x[72], -40);
    ASSERT_EQ(x[76], 916);
    ASSERT_EQ(x[996], 916);
    ASSERT_EQ(*iter, -20);
    *iter = 20;
    ASSERT_EQ(*iter, 20);
    ASSERT_EQ(x[24], 20);
}


TEST(AllocatorFixture, test11) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                      // read-only
    const pointer    b = x.allocate(247);
    ASSERT_EQ(x[0], -992);
}

TEST(AllocatorFixture, valid0) {
    using allocator_type = my_allocator<int, 1000>;
    allocator_type x;
    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
    x.allocate(5);
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x.valid(), true);
}

TEST(AllocatorFixture, valid1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                      // read-only
    const pointer    b = x.allocate(5);
    const pointer    a = x.allocate(10);
    allocator_type::iterator iter = x.begin();
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], -40);
    ASSERT_EQ(x[72], -40);
    ASSERT_EQ(x[76], 916);
    ASSERT_EQ(x[996], 916);
    ASSERT_EQ(*iter, -20);
    *iter = 20;
    ASSERT_EQ(*iter, 20);
    ASSERT_EQ(x[24], 20);
    iter++;
    *iter = 40;
    ASSERT_EQ(x.valid(), false);

}